package com.facishare.demo.redisTest;

import com.facishare.demo.pojo.entity.Student;
import com.facishare.demo.utils.RedisUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.LinkedList;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
public class StudentRedisTest {

    @Autowired
    RedisUtil redisUtil;

    @Test
    public void testSimple(){
        redisUtil.set("k1","v1");
        System.out.println(redisUtil.get("k1"));

    }

    @Test
    public void testStudent(){
        redisUtil.set("k2",new Student(1,"tset",1,null));
        Student student = (Student) redisUtil.get("k2");
        System.out.println(student.getId());
        System.out.println(student.getName());
        System.out.println(student.getAge());
    }

    @Test
    public void testListStudent(){
        List<Student> students=new LinkedList<Student>();
        students.add(new Student(1,"1",1,null));
        students.add(new Student(2,"2",2,null));
        String key="listStuds";
        boolean result = redisUtil.set(key, students);
        System.out.println(result);
        List<Student> stus = (List<Student>) redisUtil.get(key);
        System.out.println(stus);

        System.out.println(stus.get(0).getId());
        System.out.println(stus.get(0).getName());
        System.out.println(stus.get(0).getAge());
        System.out.println(stus.get(1).getId());
        System.out.println(stus.get(1).getName());
        System.out.println(stus.get(1).getAge());
    }

    @Test
    public void testNull(){
        String key="keyNull";
        redisUtil.set(key,new Student());
        Object o = redisUtil.get(key);
        System.out.println(o);
        System.out.println(o==null);
        System.out.println(o.getClass());


    }

    @Test
    public void testNull2(){
        String key="keyNull2";
        redisUtil.set(key,"");
//        redisUtil
        System.out.println(redisUtil);
    }



}
