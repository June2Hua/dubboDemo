package com.facishare.demo.studentServiceTest;

import com.facishare.demo.service.StudentService;
import com.facishare.demo.service.impl.StudentServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
public class StudentServiceTest {

    @Autowired
    StudentServiceImpl studentService;

    @Test
    public void getAllTest(){
        System.out.println(studentService.getAll());
    }

    @Test
    public void testGetStuById(){
        System.out.println(studentService.getStuById(2));
    }
}
