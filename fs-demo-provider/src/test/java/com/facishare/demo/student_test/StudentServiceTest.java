package com.facishare.demo.student_test;


import com.facishare.demo.pojo.entity.Department;
import com.facishare.demo.pojo.entity.Student;
import com.facishare.demo.pojo.result.StudentResult;
import com.facishare.demo.service.StudentService;
import com.facishare.demo.service.impl.DepartmentServiceImpl;
import com.facishare.demo.service.impl.StudentServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
public class StudentServiceTest {

    @Autowired(required = false)
    StudentServiceImpl studentServiceImpl;

    @Autowired
    DepartmentServiceImpl departmentServiceImpl;

    @Test
    public void test(){
        System.out.println(studentServiceImpl);
        List<StudentResult> students = studentServiceImpl.getAll();
        System.out.println(students);
        System.out.println(studentServiceImpl.getStuById(2));
        List<Department> departments = departmentServiceImpl.getAll();
        System.out.println(departments);
    }


}
