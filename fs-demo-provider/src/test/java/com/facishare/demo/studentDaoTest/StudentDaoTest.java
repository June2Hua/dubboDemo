package com.facishare.demo.studentDaoTest;

import com.facishare.demo.dao.StudentDao;
import com.facishare.demo.pojo.arg.StudentArg;
import com.facishare.demo.pojo.entity.Department;
import com.facishare.demo.pojo.entity.Student;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
public class StudentDaoTest {

    @Autowired
    StudentDao studentDao;

    @Test
    public void testGetAll(){
        System.out.println(studentDao.getAll());
    }

    @Test
    public void testUpdate(){
        System.out.println(studentDao.getStuById(6));
        studentDao.updateStudent(new Student(6,"666",666,new Department(6,null)));
        System.out.println(studentDao.getStuById(6));
    }

    @Test
    public void testSave(){
        studentDao.getStuById(2);
        System.out.println();
        Student student=new Student();
        student.setName("999");
        student.setAge(999);
        student.setDepartment(new Department(7,null));
        studentDao.saveStudent(student);
        System.out.println(studentDao.getAll());
    }

    @Test
    public void testDelete(){
        System.out.println(studentDao.getAll());
        studentDao.deleteStudent(9);
        System.out.println(studentDao.getAll());
    }


}
