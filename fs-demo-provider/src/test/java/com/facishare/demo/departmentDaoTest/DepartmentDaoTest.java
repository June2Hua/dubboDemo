package com.facishare.demo.departmentDaoTest;

import com.facishare.demo.dao.DepartmentDao;
import com.facishare.demo.pojo.entity.Department;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
public class DepartmentDaoTest {

    @Autowired
    DepartmentDao departmentDao;

    @Test
    public void testInsert(){
        System.out.println(departmentDao.getAll());
        Department department=new Department();
        department.setName("999");
        departmentDao.saveDepartment(department);
        System.out.println(departmentDao.getAll());
    }

    @Test
    public void testUpdate(){
        Department department=new Department(8,"8");
        departmentDao.updateDepartment(department);
        System.out.println(departmentDao.getAll());
    }

    @Test
    public void testDelete(){
        departmentDao.deleteDepartment(8);
        System.out.println(departmentDao.getAll());
    }
}
