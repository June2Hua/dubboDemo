import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;

public class Entry {

    public static void main(String[] args) throws IOException {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(new String[] {"classpath:applicationContext.xml"});
        context.start();
        System.in.read(); // 按任意键退出
    }
}
