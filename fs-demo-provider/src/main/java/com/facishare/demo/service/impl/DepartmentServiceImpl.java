package com.facishare.demo.service.impl;

import com.facishare.demo.dao.DepartmentDao;
import com.facishare.demo.pojo.entity.Department;
import com.facishare.demo.pojo.entity.Student;
import com.facishare.demo.service.DepartmentService;
import com.facishare.demo.utils.RedisUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
public class DepartmentServiceImpl implements DepartmentService {

    @Autowired
    RedisUtil redisUtil;

    Logger logger = LoggerFactory.getLogger(DepartmentServiceImpl.class);

    @Autowired
    DepartmentDao departmentDao;

    /**
     * 查询所有部门
     * 1、redis
     *      存在则返回
     *      不存在到2
     * 2、数据库
     *      数据库不存放，返回为空，同时redis缓存空结果
     *      存放，返回数据，同时写入数据库
     * @return
     */
    public List<Department> getAll() {
        //查询缓存是否存在
        String key="fs-demo-provider:departments:all:string";
        //缓存中存在
        Object object = redisUtil.get(key);
        if(object!=null){
            return (List<Department>)object;        //将list反序列化
        }
        //查询数据库所有
        List<Department> departments = departmentDao.getAll();
        if(departments==null||departments.size()==0){
            logger.warn("查询结果为空");
            //redis中缓存空结果
            redisUtil.set(key,new LinkedList<Department>().add(new Department()));
            return null;
        }

        //重新封装Dao层返回的数据
        LinkedList<Department> list=new LinkedList<Department>();
        for(Department department:departments)
            list.add(department);

        //放入缓存中
        redisUtil.set(key,list);        //将list序列化到缓存中

        return list;
    }

    /**
     * 查询单个部门
     * 1、redis
     *      存在则返回
     *      不存在到2
     * 2、数据库
     *      数据库不存放，返回为空，同时redis缓存空结果
     *      存放，返回数据，同时写入数据库
     * @return
     */
    public Department getDeptById(Integer id) {
        //查询缓存是否存在
        String key="fs-demo-provider:department:"+id+":string";
        //缓存中存在
        Object object = redisUtil.get(key);
        if(object!=null){
            return (Department) redisUtil.get(key);
        }
        //缓冲中不存在，查询数据库
        Department department = departmentDao.getDeptById(id);
        //查询为空
        if(department==null){
            logger.warn("查询结果为空 arg:{}",id);
            redisUtil.set(key,new Department());
            return null;
        }

        //重新封装Dao层返回的数据
        Department dept=new Department(department.getId(),department.getName());

        //放入缓存中
        redisUtil.set(key,dept);        //将list序列化到缓存中

        return dept;
    }
}
