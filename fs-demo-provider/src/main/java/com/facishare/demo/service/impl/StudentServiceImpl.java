package com.facishare.demo.service.impl;

import com.facishare.demo.dao.StudentDao;
import com.facishare.demo.pojo.arg.StudentArg;
import com.facishare.demo.pojo.entity.Department;
import com.facishare.demo.pojo.entity.Student;
import com.facishare.demo.pojo.result.StudentResult;
import com.facishare.demo.service.StudentService;
import com.facishare.demo.utils.RedisUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;

@Service
public class StudentServiceImpl implements StudentService {

    @Autowired
    RedisUtil redisUtil;

    @Autowired
    StudentDao studentDao;

    Logger logger = LoggerFactory.getLogger(StudentServiceImpl.class);



    /**
     * 开启事务插入数据
     */
    @Transactional
    public void saveStudent(StudentArg arg){
        Student student=new Student(arg.getId(),arg.getName(),arg.getAge(),new Department(arg.getDepartmentId(),null));
        studentDao.saveStudent(student);
    }

    /**
     * 开启事务跟新数据
     */
    @Transactional
    public void updateStudent(StudentArg arg){
        Student student=new Student(arg.getId(),arg.getName(),arg.getAge(),new Department(arg.getDepartmentId(),null));
        studentDao.updateStudent(student);
    }

    /**
     * 开启事务删除数据
     */
    @Transactional
    public void deleteStudent(Integer id){
        studentDao.deleteStudent(id);
    }

    /**
     * 查询所有学生
     * 1、redis
     *      存在则返回
     *      不存在到2
     * 2、数据库
     *      数据库不存放，返回为空，同时redis缓存空结果
     *      存放，返回数据，同时写入数据库
     * @return
     */
    public List<StudentResult> getAll() {
        //查询缓存是否存在
        String key="fs-demo-provider:student:all:string";
        Object object = redisUtil.get(key);
        //缓存中存在
        if(object!=null){
            //直接返回缓存内容
            return (List<StudentResult>)object;        //将list反序列化到缓存中
        }
        //缓存中不存在查询数据库所有
        List<Student> students = studentDao.getAll();
        //查询结果为空
        if(students==null||students.size()==0){
            logger.warn("查询结果为空,redis缓存空对象");
            //缓存空结果,即空对象
            redisUtil.set(key,new LinkedList<StudentResult>().add(new StudentResult()));
            return null;
        }

        //重新封装Dao层返回的数据
        LinkedList<StudentResult> list=new LinkedList<StudentResult>();
        for(Student student:students) {
            list.add(new StudentResult(student.getName(),student.getAge(),student.getDepartment().getName()));
        }

        //放入缓存中
        redisUtil.set(key,list);        //将list序列化到缓存中

        return list;

    }

    /**
     * 查询单个学生
     * 1、redis
     *      存在则返回
     *      不存在到2
     * 2、数据库
     *      数据库不存放，返回为空，同时redis缓存空结果
     *      存放，返回数据，同时写入数据库
     * @return
     */
    public StudentResult getStuById(Integer id) {
        //查询缓存是否存在
        String key="fs-demo-provider:"+id+":string";
        //缓存中存在
        Object object = redisUtil.get(key);
        if(object!=null){
            return (StudentResult) redisUtil.get(key);
        }
        //缓冲中不存在，查询数据库
        Student student = studentDao.getStuById(id);
        //查询为空
        if(student==null){
            logger.warn("查询结果为空 arg:{}",id);
            //缓存空对象
            redisUtil.set(key,new StudentResult());
            return null;
        }

        //重新封装Dao层返回的数据
        StudentResult stu=new StudentResult(student.getName(),student.getAge(),student.getDepartment().getName());

        //放入缓存中
        redisUtil.set(key,stu);        //将list序列化到缓存中

        return stu;

    }


}
