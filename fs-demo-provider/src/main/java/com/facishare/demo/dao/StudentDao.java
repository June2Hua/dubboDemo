package com.facishare.demo.dao;

import com.facishare.demo.pojo.arg.StudentArg;
import com.facishare.demo.pojo.entity.Student;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentDao {

    List<Student> getAll();

    Student getStuById(Integer id);

    void saveStudent(Student student);

    void updateStudent(Student student);

    void deleteStudent(Integer id);

}
