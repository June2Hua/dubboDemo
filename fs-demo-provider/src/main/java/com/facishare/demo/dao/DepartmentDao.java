package com.facishare.demo.dao;

import com.facishare.demo.pojo.entity.Department;
import com.facishare.demo.pojo.entity.Student;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DepartmentDao {

    List<Department> getAll();

    Department getDeptById(Integer id);

    void saveDepartment(Department department);

    void updateDepartment(Department department);

    void deleteDepartment(Integer id);

}
