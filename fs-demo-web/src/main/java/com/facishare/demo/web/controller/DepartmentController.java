package com.facishare.demo.web.controller;

import com.facishare.demo.pojo.entity.Department;
import com.facishare.demo.pojo.entity.Student;
import com.facishare.demo.service.DepartmentService;
import com.facishare.demo.service.StudentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

//@Controller
public class DepartmentController {

//    @Autowired
//    DepartmentService departmentService;
//
//    Logger logger = LoggerFactory.getLogger(DepartmentController.class);
//
//    /**
//     * 使用dubbo查询所有学生，调用模块com.facishare.demo.provider
//     * @return
//     */
//    @RequestMapping("/listDepartments")
//    @ResponseBody
//    public List<Department> listDepartments(){
//        List<Department> departments = departmentService.getAll();
//        if(departments==null||departments.size()==0){
//            logger.warn("查询数据为空---listStudents");
//            return null;
//        }
//        return departments;
//    }
//
//    @RequestMapping("/getdeptById/{id}")
//    @ResponseBody
//    public Department getdeptById(@PathVariable Integer id){
//        if(id==null||id<0){
//            logger.warn("参数错误，参数为空或者非法  arg:{}",id);
//            return null;
//        }
//        Department department = departmentService.getDeptById(id);
//        if(department==null){
//            logger.warn("查询数据为空  arg:{}",id);
//            return null;
//        }
//        return department;
//    }

}
