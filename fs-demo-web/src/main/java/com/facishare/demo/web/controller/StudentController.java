package com.facishare.demo.web.controller;

import com.facishare.demo.pojo.entity.Student;
import com.facishare.demo.service.StudentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class StudentController {

//    @Autowired
//    StudentService studentService;
//
//    Logger logger = LoggerFactory.getLogger(StudentController.class);
//
//    /**
//     * 使用dubbo查询所有学生，调用模块com.facishare.demo.provider
//     * @return
//     */
//    @RequestMapping("/listStudents")
//    @ResponseBody
//    public List<Student> listStudents(){
//        List<Student> students = studentService.getAll();
//        if(students==null||students.size()==0){
//            logger.warn("查询数据为空---listStudents");
//            return null;
//        }
//        return students;
//    }
//
//    @RequestMapping("/getStuById/{id}")
//    @ResponseBody
//    public Student getStuById(@PathVariable Integer id){
//        if(id==null||id<0){
//            logger.warn("参数错误，参数为空或者非法  arg:{}",id);
//            return null;
//        }
//        Student student = studentService.getStuById(id);
//        if(student==null){
//            logger.warn("查询数据为空  arg:{}",id);
//            return null;
//        }
//        return student;
//    }
//
//    @RequestMapping("/test")
//    @ResponseBody
//    public Student test(){
//        return new Student(1,"test",1);
//    }

}
