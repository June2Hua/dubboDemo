package com.facishare.demo.web.controller;

import com.facishare.demo.pojo.arg.StudentArg;
import com.facishare.demo.pojo.entity.Student;
import com.facishare.demo.pojo.result.StudentResult;
import com.facishare.demo.service.StudentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class StudentController {

    @Autowired(required = false)
    StudentService studentService;

    Logger logger = LoggerFactory.getLogger(StudentController.class);

    /**
     * 使用dubbo查询所有学生，调用模块com.facishare.demo.provider
     * @return
     */
    @RequestMapping("/listStudents")
    @ResponseBody
    public List<StudentResult> listStudents(){
        //判断dubbo是否绑定成功
        if (studentService==null){
            logger.warn("spring容器中不存在studentService");
            return null;
        }
        //查询所有
        List<StudentResult> students = studentService.getAll();
        //判断是否查询结果是否为空或者查询数据为0
        if(students==null||students.size()==0){
            logger.warn("查询数据为空---listStudents");
            return null;
        }
        return students;
    }

    /**
     * 根据路径id查询单个学生
     * @param id
     * @return
     */
    @RequestMapping("/getStuById/{id}")
    @ResponseBody
    public StudentResult getStuById(@PathVariable Integer id){
        //判断dubbo是否绑定成功
        if (studentService==null){
            logger.warn("spring容器中不存在studentService");
            return null;
        }
        if(id==null||id<0){
            logger.warn("参数错误，参数为空或者非法  arg:{}",id);
            return null;
        }
        StudentResult student = studentService.getStuById(id);
        if(student==null){
            logger.warn("查询数据为空  arg:{}",id);
            return null;
        }
        return student;
    }

    /**
     * 插入数据
     * @param arg
     */
    @RequestMapping("/insertStudent")
    public void insertStudent(@RequestBody StudentArg arg){
        //判断dubbo是否绑定成功
        if (studentService==null){
            logger.warn("spring容器中不存在studentService");
            return ;
        }
        if(arg==null){
            logger.warn("获取参数错误 arg:{}",arg);
            return ;
        }
        studentService.saveStudent(arg);
    }

    /**
     * 测试查询
     * @param id
     * @return
     */
    @RequestMapping("/test/{id}")
    @ResponseBody
    public StudentResult test(@PathVariable Integer id){

        logger.info("测试查询 arg {}",id);

        StudentResult student = studentService.getStuById(id);

        return student;
    }

}
