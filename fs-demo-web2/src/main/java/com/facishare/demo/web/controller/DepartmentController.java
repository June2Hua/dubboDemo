package com.facishare.demo.web.controller;

import com.facishare.demo.pojo.entity.Department;
import com.facishare.demo.service.DepartmentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class DepartmentController {

    @Autowired(required = false)
    DepartmentService departmentService;

    Logger logger = LoggerFactory.getLogger(DepartmentController.class);

    /**
     * 使用dubbo查询所有学生，调用模块com.facishare.demo.provider
     * @return
     */
    @RequestMapping("/listDepartments")
    @ResponseBody
    public List<Department> listDepartments(){
        //判断dubbo远程调用是否成功
        if (departmentService==null){
            logger.warn("spring容器中不存在departmentService");
            return null;
        }
        //远程调用
        List<Department> departments = departmentService.getAll();
        //判断查询结果是否为空或者数据为0
        if(departments==null||departments.size()==0){
            logger.warn("查询数据为空---listStudents");
            return null;
        }
        return departments;
    }

    @RequestMapping("/getdeptById/{id}")
    @ResponseBody
    public Department getdeptById(@PathVariable Integer id){
        //判断远程dubbo远程调用是否成功
        if (departmentService==null){
            logger.warn("spring容器中不存在departmentService");
            return null;
        }
        //判断参数输入
        if(id==null||id<0){
            logger.warn("参数错误，参数为空或者非法  arg:{}",id);
            return null;
        }
        //dubbo远程调用
        Department department = departmentService.getDeptById(id);
        //查询结构为空
        if(department==null){
            logger.warn("查询数据为空  arg:{}",id);
            return null;
        }
        return department;
    }

}
