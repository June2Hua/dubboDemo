package com.facishare.demo.pojo.arg;

import java.io.Serializable;

/**
 * 插入数据或者跟新数据时，向后台传入的学生参数
 */
public class StudentArg implements Serializable {

    private Integer id;//学生的id

    private String name;//学生的名字

    private Integer age;//学生年龄

    private Integer departmentId;//选择的部门对应的id

    public StudentArg() {
    }

    public StudentArg(Integer id, String name, Integer age, Integer departmentId) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.departmentId = departmentId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Integer departmentId) {
        this.departmentId = departmentId;
    }

    @Override
    public String toString() {
        return "StudentArg{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", departmentId=" + departmentId +
                '}';
    }
}
