package com.facishare.demo.pojo.result;

import java.io.Serializable;

/**
 * 返回前端的Student数据,隐藏id信息
 */
public class StudentResult implements Serializable {

    private String studentName;

    private Integer age;

    private String departmentName;//部门信息

    public StudentResult() {
    }

    public StudentResult(String studentName, Integer age, String departmentName) {
        this.studentName = studentName;
        this.age = age;
        this.departmentName = departmentName;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }
}
