package com.facishare.demo.pojo.result;

import java.io.Serializable;

public class DepartmentResult implements Serializable {

    private String departmentName;

    public DepartmentResult() {
    }

    public DepartmentResult(String departmentName) {
        this.departmentName = departmentName;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    @Override
    public String toString() {
        return "DepartmentResult{" +
                "departmentName='" + departmentName + '\'' +
                '}';
    }
}
