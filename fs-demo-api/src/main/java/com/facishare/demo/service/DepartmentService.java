package com.facishare.demo.service;

import com.facishare.demo.pojo.entity.Department;

import java.util.List;

public interface DepartmentService {

    /**
     * 查询所有的部门
     * @return
     */
    List<Department> getAll();

    /**
     * 根据id查询单个部门
     * @param id
     * @return
     */
    Department getDeptById(Integer id);
}
