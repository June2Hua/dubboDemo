package com.facishare.demo.service;

import com.facishare.demo.pojo.arg.StudentArg;
import com.facishare.demo.pojo.entity.Student;
import com.facishare.demo.pojo.result.StudentResult;

import java.util.List;

public interface StudentService {

    List<StudentResult> getAll();

    StudentResult getStuById(Integer id);

    void saveStudent(StudentArg arg);

    void updateStudent(StudentArg arg);

    void deleteStudent(Integer id);
}
